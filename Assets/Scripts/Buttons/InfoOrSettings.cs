﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class InfoOrSettings : MonoBehaviour
{
    public void Informations()
    {
        SceneManager.LoadScene(1);
    }

    public void Settings()
    {
        SceneManager.LoadScene(2);
    }
}
