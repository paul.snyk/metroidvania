﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FBoss_laser : MonoBehaviour
{
    void Start()
    {
        Destroy(gameObject, 1f);
    }
    //en cas de contact avec le joueur, celui perd de la vie
    public void OnCollisionEnter2D(Collision2D col)
    {
        col.gameObject.GetComponent<PlayerLife>().DamagePlayer(1);
        Destroy(gameObject);
    }
}
