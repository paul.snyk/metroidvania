﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class finalboss : MonoBehaviour
{
    public int finalBossHp;
    public float speed;
    public int status;
    public Transform FirePoint1;
    public Transform FirePoint2;
    public GameObject boss_bullet;
    public GameObject boss_laser, sliderFinalBoss, explosion;
    public float atkspeed1;
    public float atkspeed2;
    public float atkspeedtimer;
    public Slider sliderBoss;
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        status = 1;
        finalBossHp = 25 * GetComponent<EnemyLife>().enemyKilleds;
    }

    // Update is called once per frame
    void Update()
    {
        if (atkspeedtimer > 0.8)
            animator.SetBool("isFBossAtking", false);
        atkspeedtimer += Time.deltaTime;
        if (finalBossHp > 400)
        {
            finalboss_mvt();
            finalboss_atk_1();
        }
        if (finalBossHp <= 400)
        {
            finalboss_mvt();
            finalboss_atk_2();
        }

        if (finalBossHp <= 0)
        {
            Die();
        }
    }

    //invocation des balles
    private void finalboss_atk_1()
    {
        if (atkspeedtimer > atkspeed1)
        {
            animator.SetBool("isFBossAtking", true);
            Instantiate(boss_bullet, FirePoint1.position, Quaternion.identity);
            atkspeedtimer = 0;

        }
    }
    //invocation du rayon laser
    private void finalboss_atk_2()
    {
        if (atkspeedtimer > atkspeed2)
        {
            animator.SetBool("isFBossAtking", true);
            Instantiate(boss_laser, FirePoint2.position, Quaternion.identity, FirePoint2);
            atkspeedtimer = 0;
        }
    }
    //déplacement du boss en flottant de haut en bas
    private void finalboss_mvt()
    {
        if (gameObject.transform.position.y >= 170 && status == 1)
            status = 2;
        else if (gameObject.transform.position.y <= 165.6 && status == 2)
            status = 1;
        if (status == 1)
        {
            gameObject.transform.Translate(Vector3.up * speed * Time.deltaTime);
        }
        else if (status == 2)
        { 
            gameObject.transform.Translate(-Vector3.up * speed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("FBoss_Bullet"))
            Destroy(other.gameObject);
        //PERDRE VIE
        //PERDRE VIE DU JOUEUR
    }
    
    float CalculateValueBoss()
    {
        return finalBossHp;
    }
    
    public void DamageBoss3(int enemyDamage)  // Tremblement de caméra + explosion + scale de l'ennemi lors de la mort
    {
        Debug.Log("J'touche false ");
        sliderFinalBoss.SetActive(true);
        sliderBoss.value = CalculateValueBoss();
        finalBossHp -= enemyDamage;
        StartCoroutine(Blink());
    }
    IEnumerator Blink() // Change la couleur du sprite en rouge pendant 0.2 secondes quand l'ennemi se fait frapper
    {
        GetComponent<SpriteRenderer>().color = new Color(1, 0, 0);
        yield return new WaitForSeconds(0.2f);
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
    }
    public void Die()
    {
        sliderFinalBoss.SetActive(false);
        explosion.SetActive(true);
        //transform.DOScale(0, 0.25f);
        GameObject explode = Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(explode, 0.1f);
        //m_Rigidbody.AddForce (new Vector2 (0, 0));
        Destroy(gameObject, 0.2f);
        SceneManager.LoadScene(4);
    }
}
    
