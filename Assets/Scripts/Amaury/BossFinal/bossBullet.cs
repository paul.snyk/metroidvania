﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossBullet : MonoBehaviour
{
    public float speed;
    public Rigidbody2D rb;
    void Start()
    {
        rb.velocity = transform.right * speed;
        Destroy(gameObject, 10f);
    }

    //en cas de contact avec le joueur, celui perd de la vie
    public void OnCollisionEnter2D(Collision2D col)
    {
        col.gameObject.GetComponent<PlayerLife>().DamagePlayer(1);
        Destroy(gameObject);
    }
    //IF HIT BY PLAYERSWORD THEN REPARTIR DANS L'AUTRE SENS
    //AVEC UN SPEED  * - 1 + 1
    //SINON DESTROYED
}
