﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Right_attackPointP2 : MonoBehaviour
{
	public float speed;
	private int status;
	void Start()
	{
		status = 2;
	}

	//déplacement de la cible gauche que les plumes suivent
	void Update()
	{
		if (gameObject.transform.position.x >= 124 && status == 1)
			status = 2;
		else if (gameObject.transform.position.x <= 112 && status == 2)
			status = 1;
		else if (status == 1)
		{
			gameObject.transform.Translate(Vector3.right * speed * Time.deltaTime);
		}
		else if (status == 2)
		{
			gameObject.transform.Translate(-Vector3.right * speed * Time.deltaTime);
		}
	}
}