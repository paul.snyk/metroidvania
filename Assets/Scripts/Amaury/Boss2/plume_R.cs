﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plume_R : MonoBehaviour
{
    public Transform target;
    private Vector2 initD;
    public float speed;

    void Start()
    {
        target = GameObject.Find("TargetR").transform;
        initD = target.position;
        Destroy(gameObject, 2f);
    }
    //déplacement vers la cible qui bouge
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
    }
    //fait perdre de la vie au joueur en cas de contact
    public void OnCollisionEnter2D(Collision2D col)
    {
        col.gameObject.GetComponent<PlayerLife>().DamagePlayer(1);
        Destroy(gameObject);
    }
}
