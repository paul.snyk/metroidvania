﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChimeraPattern : MonoBehaviour
{
    public bool jumpBoostChimera;
    public Transform Lspawner;
    public Transform Rspawner;
    public GameObject ChimeraAtkL;
    public GameObject ChimeraAtkR;
    public GameObject ChimeraAtkPlayer;
    public GameObject chimera;
    public GameObject healthBarUIBoss, explosion, portal;
    public Slider sliderBoss;
    private Camera CameraBoss2;
    public float speed;
    public float chimeraHP, maxHP;
    private int status;
    public bool smallSizeBuff;

    public GameObject player;
    private float AttackSpeedTimer;
    public float AttackSpeed;
    public float BaseAttackSpeed;
    public void Start()
    {
        smallSizeBuff = false;
        healthBarUIBoss.SetActive(false);
        chimeraHP = maxHP;
        explosion.SetActive(false);
        sliderBoss.value = CalculateValueBoss();
        AttackSpeed = BaseAttackSpeed;
        status = 2;
    }

    public void Update()
    {
        if (chimeraHP > 0)
        {
            Chimera_atk();
            Chimera_mvt();
        }
        //activation du jump boost à la mort du boss, tout en invoquant une dernière fois une attaque, en activant un portail
        else if (chimeraHP <= 0)
        {
            jumpBoostChimera = true;
            Instantiate(ChimeraAtkPlayer, Lspawner.position, Quaternion.identity);
            Instantiate(ChimeraAtkL, Lspawner.position, Quaternion.identity);
            Instantiate(ChimeraAtkR, Rspawner.position, Quaternion.identity);
            DestroyImmediate(gameObject);
            player.GetComponent<PlayerMovement>().JumpBoost();
            portal.SetActive(true);
            player.GetComponent<PlayerMovement>().smallSizeBuff = true;
            healthBarUIBoss.SetActive(false);
            Die();
        }
    }
    
    //invocation des plumes en fonction d'un timer 
    private void Chimera_atk()
    {
        AttackSpeedTimer += Time.deltaTime;
        if (AttackSpeedTimer > AttackSpeed)
        {
            Instantiate(ChimeraAtkPlayer, Lspawner.position, Quaternion.identity);
            Instantiate(ChimeraAtkL, Lspawner.position, Quaternion.identity);
            Instantiate(ChimeraAtkR, Rspawner.position, Quaternion.identity);
            AttackSpeedTimer = 0f;
            AttackSpeed = BaseAttackSpeed;
            AttackSpeed = AttackSpeed + (Random.Range(AttackSpeed / -2, AttackSpeed / 2));
        }
    }

    //fonction qui fait qu'elle flotte de haut en bas
    private void Chimera_mvt()
    {
        if (gameObject.transform.position.y >= 78 && status == 1)
            status = 2;
        else if (gameObject.transform.position.y <= 72 && status == 2)
            status = 1;
        if (status == 1)
            gameObject.transform.Translate(Vector3.up * speed * Time.deltaTime);
        else if (status == 2)
            gameObject.transform.Translate(-Vector3.up * speed * Time.deltaTime);
    }
    
    //actualisation de la vie du boss
    float CalculateValueBoss()
    {
        return chimeraHP;
    }
    
    public void DamageBoss2(int enemyDamage)  // Tremblement de caméra + explosion + scale de l'ennemi lors de la mort
    {
        Debug.Log("J'touche false ");
        chimeraHP -= enemyDamage;
        healthBarUIBoss.SetActive(true);
        sliderBoss.value = CalculateValueBoss();
        StartCoroutine(Blink());
    }
    IEnumerator Blink() // Change la couleur du sprite en rouge pendant 0.2 secondes quand l'ennemi se fait frapper
    {
        GetComponent<SpriteRenderer>().color = new Color(1, 0, 0);
        yield return new WaitForSeconds(0.2f);
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
    }
    //mort du boss
    public void Die()
    {
        player.GetComponent<PlayerLife>().AddHealth(5);
        healthBarUIBoss.SetActive(false);
        SmallSizeBuff();
        explosion.SetActive(true);
        portal.SetActive(true);
        //transform.DOScale(0, 0.25f);
        GameObject explode = Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(explode, 1.5f);
        //m_Rigidbody.AddForce (new Vector2 (0, 0));
        Destroy(gameObject, 0.2f);
    }

    public void SmallSizeBuff()
    {
        smallSizeBuff = true;
    }
}
