﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class skeleton_boss : MonoBehaviour
{
    public int boss_hp, maxBossHP;
    private Rigidbody2D m_Rigidbody;
    
    public float force1;
    public float force2;
    public float boss_cd1;
    public float boss_cd2;
    public float AtkTimer;
    public bool inTheAir, playerBuff;
    
    public GameObject LShockwave;
    public GameObject RShockwave;
    public GameObject explosion, player, sliderB, portal;
    public Transform LSpawner;
    public Transform RSpawner;
    public Slider sliderBoss;
    

    public Camera mainCamera;

    public Animator animator;
    
    // Start is called before the first frame update
    void Start()
    {
        sliderB.SetActive(false);
        explosion.SetActive(false);
        inTheAir = false;
        m_Rigidbody = GetComponent<Rigidbody2D>();
        boss_hp = maxBossHP;
        sliderBoss.value = CalculateValueBoss();
    }

    void Update()
    {
        AtkTimer = AtkTimer + Time.deltaTime;
        //si le boss a plus de 100hp, il saute et active un booléen
        if (boss_hp > 100 && boss_cd1 < AtkTimer)
        {
            animator.SetBool("SkeBossAtk", true);
            AtkTimer = 0;
            m_Rigidbody.AddForce (new Vector2 (0, force1));
            inTheAir = true;
        }
        //si il a moins de 100hp, il effectue les actions d'au dessus plus rapidement
        else if (boss_hp <= 100 && boss_cd2 < AtkTimer)
        {
            animator.SetBool("SkeBossAtk", true);
            AtkTimer = 0;
            m_Rigidbody.AddForce (new Vector2 (0, force2));
            inTheAir = true;
        }
        //quand le boss meurt le player récupère un power up et ca active un tuto, tout en activant un portail
        if (boss_hp <= 0)
        {
            player.GetComponent<PlayerAttack>().PlayerBuffForce();
            player.GetComponent<TutoCommande>().TutoAfterBoss();
            portal.SetActive(true);
            Die();
        }
    }

    //en case de collision avec le player celui ci perd de la vie, si le boss touche le sol avec le booléen de saut activer il invoque 2 shockwaves
    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            sliderB.SetActive(true);
            sliderBoss.value = CalculateValueBoss();
            GetComponent<PlayerLife>().DamagePlayer(1);
        }
        if (inTheAir)
        {
            animator.SetBool("SkeBossAtk", false);
            Instantiate(LShockwave, LSpawner.position, Quaternion.identity);
            Instantiate(RShockwave, RSpawner.position, Quaternion.identity);
            mainCamera.DOShakePosition (0.5f, 0.01f, 100, 90, false);
            mainCamera.DOShakeRotation(0.5f, 0.01f, 100, 90, false);
            inTheAir = false;
        }
    }
    
    //mise à jour du slider d'hp
    float CalculateValueBoss()
    {
        return boss_hp;
    }
    
    public void Damage(int enemyDamage)  // Tremblement de caméra + explosion + scale de l'ennemi lors de la mort
    {
        Debug.Log("J'touche false ");
        sliderB.SetActive(true);
        sliderBoss.value = CalculateValueBoss();
        boss_hp -= enemyDamage;
        StartCoroutine(Blink());
    }
    IEnumerator Blink() // Change la couleur du sprite en rouge pendant 0.2 secondes quand l'ennemi se fait frapper
    {
        GetComponent<SpriteRenderer>().color = new Color(1, 0, 0);
        yield return new WaitForSeconds(0.2f);
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
    }
    
    //mort du boss
    public void Die()
    {
        player.GetComponent<PlayerLife>().AddHealth(3);
        sliderB.SetActive(false);
        explosion.SetActive(true);
        //transform.DOScale(0, 0.25f);
        GameObject explode = Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(explode, 0.1f);
        //m_Rigidbody.AddForce (new Vector2 (0, 0));
        Destroy(gameObject, 0.2f);
                
        
    }
}
