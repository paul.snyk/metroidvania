﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShockwaveL : MonoBehaviour
{
	private Rigidbody2D rb;
	public float speed;
	public float DestroyTimer;

	// Start is called before the first frame update
	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
		rb.velocity = -transform.right * speed;
		Destroy(gameObject, DestroyTimer);
	}

	//si l'objet touche quelque chose il se détruit
	public void OnCollisionEnter2D(Collision2D col)
	{
		col.gameObject.GetComponent<PlayerLife>().DamagePlayer(1);
		Destroy(gameObject);
	}
}