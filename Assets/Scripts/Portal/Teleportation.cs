﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using TMPro;
using UnityEngine;

public class Teleportation : MonoBehaviour
{

    public GameObject portal, player;
    public bool canTeleport = true;

    public void OnTriggerEnter2D(Collider2D other) //Si le joueur entre en collision avec le portail lance la coroutine
    {
        if (other.gameObject.CompareTag("Player") && canTeleport == true)
        {
            StartCoroutine(Teleport());
        }
    }

    IEnumerator Teleport() //Attends 0.5 secondes et teleporte le joueur à la position du portail
    {
        yield return new WaitForSeconds(1f);
        player.transform.position = new Vector2(portal.transform.position.x, portal.transform.position.y);
        canTeleport = false;
        StartCoroutine(TeleportationDuration());
    }

    IEnumerator TeleportationDuration()
    {
        yield return new WaitForSeconds(3f);
        canTeleport = true;
    }
}
