﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public Transform attackPoint;
    public Animator animator;
    //public float rangeAttack = 0.5f;
    public LayerMask enemyLayers;
    public LayerMask bossLayers ,boss2Layers ,boss3Layers;
    public LayerMask obstacleLayers;
    private float timeAppelScript = 0.1f;
    public GameObject explosionAttack, loadExplosion;
    public Camera mainCamera;  
    public bool playerBuffForce = false;
    public float attackRangeX, attackRangeY;
    //public GameObject wall, wall2, wall3, wall4;
    public int lifeWall = 3;
    public int lifeWall2 = 3;
    public int lifeWall3 = 3;
    public int lifeWall4 = 3;
    public bool wallContact;
    public AudioSource sword_sound;
    public bool smallSizeBuff = false;
    void Start()
    {
        explosionAttack.SetActive(false);
    }

    void Update()
    {
        animator.SetFloat("Attack", 0);// on update en continue le float de l'anim de l'attack du player a zero
        if (Input.GetMouseButtonDown(0) || (Input.GetKeyDown(KeyCode.Z)))//si on appuie sur clic gauche souris ou touche z alor son attack avec le player
        {
            sword_sound.Play();
            animator.SetFloat("Attack", 1);//activation anime player attack
            AttackPlayer();//on appel void attackplayer
            AttackObstacle(); //on appel void attackobstacle
            Debug.Log("attack obstacle void");
            AttackPlayerBoss();//on appel attackplayerboss
            AttackPlayerBoss2();//on appel attackplayerboss2
            AttackPlayerBoss3();//on appel attackplayerboss3
            
            explosionAttack.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.P))// si input p alors on recup la dernière save et on l'applique sur le jeu
        {
            StartCoroutine(WaitForLoad());
        }
        if (Input.GetKeyDown(KeyCode.A))// si input a alors on save le jeu
        {
            SaveSystemPlayerAttack.SavePlayerAttack(this);
        }
        
    }

    public void AttackPlayer()
    {
        // lorsque collision avec un object on va check le layers et si layers ok avec les layers intégrer dans le player alors
        Collider2D[] hitenemy = Physics2D.OverlapBoxAll(attackPoint.position, new Vector2(attackRangeX, attackRangeY), 0, enemyLayers);
        //on instance l'explosion attaque lorsque le joueur frappe
        GameObject ps = Instantiate(explosionAttack, attackPoint.position, Quaternion.identity);
        mainCamera.DOShakePosition(0.2f, 0.02f, 100, 90, false);
        //on detruit l'explosion attaque

        Destroy(ps, 0.4f);
        //boucle if regarde collider de la cible

        for (int i = 0; i < hitenemy.Length; i++)
        {
            if (playerBuffForce == false)
            {//inflige 1 damage à la cible
                hitenemy[i].GetComponent<EnemyLife>().Damage(1);
                //Debug.Log("activation damage 1");
            }
            else if (playerBuffForce == true)
            {//inflige 4 damage à la cible
                hitenemy[i].GetComponent<EnemyLife>().Damage(4);
                // Debug.Log("activation damage 4");
            }
        }
        
    }

    void AttackPlayerBoss()
    {
        //void attaqkplayerboss a le même fonctionnement que la void attackplayer
        Collider2D[] hitenemy = Physics2D.OverlapBoxAll(attackPoint.position, new Vector2(attackRangeX, attackRangeY), 0, bossLayers);
        GameObject ps = Instantiate(explosionAttack, attackPoint.position, Quaternion.identity);
        mainCamera.DOShakePosition(0.2f, 0.02f, 100, 90, false);

        Destroy(ps, 0.4f);

        for (int i = 0; i < hitenemy.Length; i++)
        {
            if (playerBuffForce == false)
            {
                hitenemy[i].GetComponent<skeleton_boss>().Damage(10);
                wallContact = true;
                //Debug.Log("activation damage 1");
            }
            else if (playerBuffForce == true)
            {
                hitenemy[i].GetComponent<skeleton_boss>().Damage(40);
                wallContact = true;
                // Debug.Log("activation damage 4");
            }
        }
    }

    void AttackObstacle()
    {
        //cette void a le même fonctionnement que la void attackplayer
        Debug.Log("debut void attack obstacle");
        Collider2D[] hitenemy = Physics2D.OverlapBoxAll(attackPoint.position, new Vector2(attackRangeX, attackRangeY), 0, obstacleLayers);
        Debug.Log("avant boucle for");
        for (int i = 0; i < hitenemy.Length; i++)
        {
            if (wallContact == true)
            {
                Debug.Log("contact " + hitenemy);
                hitenemy[i].GetComponent<ButtonWall>().DamageWall(1);
                //Debug.Log("activation damage 1");
            }
            
        }
    }
    
    public void AttackPlayerBoss2()
    {
        //cette void a le même fonctionnement que la void attackplayer
        Collider2D[] hitenemy = Physics2D.OverlapBoxAll(attackPoint.position, new Vector2(attackRangeX, attackRangeY), 0, boss2Layers);
        GameObject ps = Instantiate(explosionAttack, attackPoint.position, Quaternion.identity);
        mainCamera.DOShakePosition(0.2f, 0.02f, 100, 90, false);

        Destroy(ps, 0.4f);

        for (int i = 0; i < hitenemy.Length; i++)
        {
            if (playerBuffForce == false)
            {
                hitenemy[i].GetComponent<ChimeraPattern>().DamageBoss2(10);
                wallContact = true;
                GetComponent<PlayerMovement>().SmallSizeBuffPlayer();
                //Debug.Log("activation damage 1");
            }
            else if (playerBuffForce == true)
            {
                hitenemy[i].GetComponent<ChimeraPattern>().DamageBoss2(40);
                wallContact = true;
                GetComponent<PlayerMovement>().SmallSizeBuffPlayer();
                // Debug.Log("activation damage 4");
            }
        }
    }
    void AttackPlayerBoss3()
    {
        //cette void a le même fonctionnement que la void attackplayer
        Collider2D[] hitenemy = Physics2D.OverlapBoxAll(attackPoint.position, new Vector2(attackRangeX, attackRangeY), 0, boss3Layers);
        GameObject ps = Instantiate(explosionAttack, attackPoint.position, Quaternion.identity);
        mainCamera.DOShakePosition(0.2f, 0.02f, 100, 90, false);

        Destroy(ps, 0.4f);

        for (int i = 0; i < hitenemy.Length; i++)
        {
            if (playerBuffForce == false)
            {
                hitenemy[i].GetComponent<finalboss>().DamageBoss3(10);
                wallContact = true;
                //Debug.Log("activation damage 1");
            }
            else if (playerBuffForce == true)
            {
                hitenemy[i].GetComponent<finalboss>().DamageBoss3(40);
                wallContact = true;
                // Debug.Log("activation damage 4");
            }
        }
    }
    
    public void ExplosionAttack()
    {
        Destroy(explosionAttack);
    }
    private void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
        {
            return;
        } 
        // Gizmos.DrawSphere(attackPoint.position, rangeAttack);
    }
    
    public void SaveLife()
    {
        SaveSystemPlayerAttack.SavePlayerAttack(this);
    }

    public void LoadLife()
    {
        PlayerDataAttack dataAttack = SaveSystemPlayerAttack.LoadPlayerAttack();

        smallSizeBuff = dataAttack.smallSizeBuff;
        playerBuffForce = dataAttack.playerBuffForce;
        wallContact = dataAttack.wallContact;
    }
    
    public void PlayerBuffForce()
    {
        playerBuffForce = true;
    }

    IEnumerator WaitForLoad()
    {//systeme de save des infos du player débloquer durant l'aventure
        yield return new WaitForSeconds(2);
        PlayerDataAttack dataAttack = SaveSystemPlayerAttack.LoadPlayerAttack();
        
        smallSizeBuff = dataAttack.smallSizeBuff;
        playerBuffForce = dataAttack.playerBuffForce;
        wallContact = dataAttack.wallContact;
    }
    
}

