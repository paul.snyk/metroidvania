﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour
{
    public GameObject gameOverPanel;//panel game over
    public GameObject[] gameOverLettre;//tableau gameobject lettre
    public GameObject buttonReset;//gameobject button retry

    public void Start()
    {
        
    }

    public void RunGameOver()
    {
        Debug.Log("c'est le gameover");//c'est un debug log
        gameOverPanel.SetActive(true);//activation panel game over
        gameOverPanel.GetComponent<Image>().DOFade(1, 2).OnComplete(SequenceLettre);//changement fade panel game over 
    }
    public void SequenceLettre()
    {
        Sequence ouiMonSeigneur = DOTween.Sequence();//creation sequence dotween
        ouiMonSeigneur.Append(gameOverLettre[0].GetComponent<Image>().DOFade(1, 0.2f));//apparition des lettre du pannel game over une par une
        ouiMonSeigneur.Append(gameOverLettre[1].GetComponent<Image>().DOFade(1, 0.2f));//apparition des lettre du pannel game over une par une
        ouiMonSeigneur.Append(gameOverLettre[2].GetComponent<Image>().DOFade(1, 0.2f));//apparition des lettre du pannel game over une par une
        ouiMonSeigneur.Append(gameOverLettre[3].GetComponent<Image>().DOFade(1, 0.2f));//apparition des lettre du pannel game over une par une
        ouiMonSeigneur.Append(gameOverLettre[4].GetComponent<Image>().DOFade(1, 0.2f));//apparition des lettre du pannel game over une par une
        ouiMonSeigneur.Append(gameOverLettre[5].GetComponent<Image>().DOFade(1, 0.2f));//apparition des lettre du pannel game over une par une
        ouiMonSeigneur.Append(gameOverLettre[6].GetComponent<Image>().DOFade(1, 0.2f));//apparition des lettre du pannel game over une par une
        ouiMonSeigneur.Append(buttonReset.GetComponent<Image>().DOFade(1, 2));//apparition button retry
        
    }
}
