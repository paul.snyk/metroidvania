﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonWall : MonoBehaviour
{
    //script placé sur chaque wall avec en plus un layers précis
    public float lifeWall;//public float des pv du wall

    private void Update()
    {
        if (lifeWall <= 0)//si pv du wall tombe a zero alors
        {
            Destroy(gameObject);// destruction du wall
        }
    }

    public void DamageWall(float value)
    {//void pour reception des degas de l'attack du player depuis script playerattack
        lifeWall -= value;
    }
}
