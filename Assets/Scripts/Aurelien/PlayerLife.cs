﻿using System.Collections;
using System.Security.Cryptography;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLife : MonoBehaviour
{
    public int lifePlayer, maxlifePlayer;
    private GameOverScript gameOverLink;
    public Slider slider;
    public GameObject healthBarUI, explosion, miniMapPosition;
    public Camera mainCamera, secondeCamera, thirdCamera;
    public bool miniMap;

    public PlayerLife savePlayer;
    // Start is called before the first frame update
    void Start()
    {
        //buffForcePlayer.GetComponent<PlayerAttack>().PlayerBuffForce();
        secondeCamera.enabled = false;
        thirdCamera.enabled = false;
        miniMap = false;
        gameOverLink = GetComponent<GameOverScript>();
        lifePlayer = maxlifePlayer;
        slider.value = CalculateValue();
    }
    void Update()
    {
        slider.value = CalculateValue();
        if (Input.GetKeyDown(KeyCode.P))//si pression sur touche p alors on load l'ancienne sauvegarde
        {
            StartCoroutine(WaitForLoad());
        }
        if (Input.GetKeyDown(KeyCode.A))//si touche a activé alors on save la partie
        {
            SaveSystemPlayerLife.SavePlayerLife(this);
        }
        if (lifePlayer < maxlifePlayer)
        {
            healthBarUI.SetActive(true);
        }
        if (lifePlayer > maxlifePlayer)
        {
            lifePlayer = maxlifePlayer;
        }

        if (Input.GetKeyDown(KeyCode.M) && miniMap == false)//activation de la minimap
        {
            mainCamera.enabled = true;
            thirdCamera.enabled = false;
            miniMap = true;
        }
        else if (Input.GetKeyDown(KeyCode.M) && miniMap == true)//desactivation de la mini map
        {
            mainCamera.enabled = false;
            thirdCamera.enabled = true;
            miniMap = false;
            GameObject miniMapP = Instantiate(miniMapPosition, transform.position, Quaternion.identity);
            Destroy(miniMapP, 3);
        }
        playerLife();
    }
    void playerLife()
    {
        if (lifePlayer <= 0)
        {
            gameOverLink.RunGameOver();
            healthBarUI.SetActive(false);
            Die();
        }
    }
    public void DamagePlayer(int value)
    {
        lifePlayer -= value;
        mainCamera.DOShakePosition (0.5f, 0.05f, 200, 45, true);
        StartCoroutine(Blink());
    }

    public void Die()
    {
        lifePlayer = 0;
        gameOverLink.RunGameOver();
        secondeCamera.enabled = true;
        secondeCamera.DOShakePosition (0.5f, 0.05f, 100, 90, false);
        secondeCamera.DOShakeRotation(0.5f, 0.05f, 100, 90, false);
        explosion.SetActive(true);
        transform.DOScale(0, 0.25f);
        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    public void SaveLife()
    {
        SaveSystemPlayerLife.SavePlayerLife(this);
    }

    public void LoadLife()
    {
        PlayerDataLife dataLife = SaveSystemPlayerLife.LoadPlayerLife();

        //buffForcePlayer = dataLife.buffForcePlayer;
        lifePlayer = dataLife.lifePlayer;
        slider.value = CalculateValue();
    }
    float CalculateValue()
    {
        return lifePlayer;
    }

    IEnumerator WaitForLoad()
    {
        yield return new WaitForSeconds(2);
        PlayerDataLife dataLife = SaveSystemPlayerLife.LoadPlayerLife();

        lifePlayer = dataLife.lifePlayer;
        slider.value = CalculateValue();
    }
    
    IEnumerator Blink() // Change la couleur du sprite en rouge pendant 0.2 secondes quand l'ennemi se fait frapper
    {
        GetComponent<SpriteRenderer>().color = new Color(1, 0, 0);
        yield return new WaitForSeconds(0.2f);
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
    }

    public void AddHealth(int amountHealth)
    {
        //recuperation des points de vie du player lorsque boss meurt
        lifePlayer += amountHealth;
        slider.value = CalculateValue();
    }
}
