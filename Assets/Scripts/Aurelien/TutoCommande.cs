﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

public class TutoCommande : MonoBehaviour
{
    public TMP_Text textTuto;
    public Image[] imageTuto;

    public bool step;
    public bool step1;
    public bool step2;
    public bool step3;
    public bool step4;
    public bool stepAfterBoss;
    public bool stepAfterBoss2;

    public Image imageWall;
    public Image imageWall2;
    public GameObject boxtuto;
    
    public void Start()
    {
        textTuto.text = "Q ou Left Arrow pour aller vers la Gauche \n D ou Right Arrow pour aller vers la Droite \n \n Deplacez-vous pour passer à la suite";// on remplis text mesh pro avec un text

        step = true;//passage bool en true
    }

    public void Update()
    {
        if (step == true)//check si bool en true
        {
            if (Input.GetButtonDown("Horizontal"))//si on appuie sur le stouche dans l'input setting avec le horizontal alors
            {
                textTuto.text = "";// on vide le text
                Sequence ouiMonSeigneur = DOTween.Sequence();// creation sequence dotween
                ouiMonSeigneur.Append(imageTuto[6].GetComponent<Image>().DOFade(0, 0f));//disparition touche deplacement de l'écran
                ouiMonSeigneur.Append(imageTuto[3].GetComponent<Image>().DOFade(0, 0f));//disparition touche deplacement de l'écran
                ouiMonSeigneur.Append(imageTuto[1].GetComponent<Image>().DOFade(0, 0f));//disparition touche deplacement de l'écran
                ouiMonSeigneur.Append(imageTuto[2].GetComponent<Image>().DOFade(0, 0f));//disparition touche deplacement de l'écran
                step1 = true;//activation bool pour etape d'après dans tuto
                ouiMonSeigneur.Append(imageTuto[8].GetComponent<Image>().DOFade(1, 0));//apparition touche z à l'écran
                step = false;//bool step passage en false
            }  
        }
        if (step1 == true)//si bool en true alors
        {
            textTuto.text = "Pour attaquer , appuyer sur Z \n Faite un essai de l'attaque de votre personnage pour continuer";
            if (Input.GetKeyDown(KeyCode.Z))// si joueur appuie sur z alors
            {
                Sequence deuxiemeSequence = DOTween.Sequence();// creation sequence dotween
                deuxiemeSequence.Append(imageTuto[8].GetComponent<Image>().DOFade(0, 0));//disparition image touche z attack player
                deuxiemeSequence.Append(imageTuto[0].GetComponent<Image>().DOFade(1, 0));//apparition image touche P pour reload
                deuxiemeSequence.Append(imageTuto[5].GetComponent<Image>().DOFade(1, 0));//apparition image touche A pour save
                step2 = true;//true deuxieme step
                textTuto.text = "Pour Sauvegarder : appuyer sur A \n Pour Reprendre la derniere sauvegarde : appuyer sur P \n \n Pour continuer dans les explications appuyer sur S ";
                step1 = false;//false premoière step
            }
        }

        if (step2 == true)
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                Sequence troisiemeSequence = DOTween.Sequence();
                troisiemeSequence.Append(imageTuto[0].GetComponent<Image>().DOFade(0, 0));//globalement toujours du fade apparition ou disparition
                troisiemeSequence.Append(imageTuto[5].GetComponent<Image>().DOFade(0, 0));//globalement toujours du fade apparition ou disparition
                troisiemeSequence.Append(imageTuto[7].GetComponent<Image>().DOFade(1, 0));//globalement toujours du fade apparition ou disparition
                step3 = true;
                textTuto.text = "Pour sauter : appuyer sur Space \n Essayer donc le saut de votre personnage pour continuer le tutoriel";
                step2 = false;
            }
        }

        if (step3 == true)
        {
            if (Input.GetButtonDown("Jump"))
            {
                Sequence quatriemeSequence = DOTween.Sequence();
                quatriemeSequence.Append(imageTuto[7].GetComponent<Image>().DOFade(0, 0));//globalement toujours du fade apparition ou disparition
                quatriemeSequence.Append(imageTuto[4].GetComponent<Image>().DOFade(1, 0));//globalement toujours du fade apparition ou disparition
                textTuto.text = "Pour afficher la carte : appuyer sur M";
                step4 = true;
                step3 = false;
            }
        }

        if (step4 == true)
        {
            if (Input.GetKeyDown(KeyCode.M))
            {
                Sequence finalSequence = DOTween.Sequence();
                finalSequence.Append(imageTuto[4].GetComponent<Image>().DOFade(0, 0));//globalement toujours du fade apparition ou disparition
                textTuto.text = "";
                step3 = false;
            }
        }

        if (stepAfterBoss == true)
        {
            textTuto.text = "Vous pouvez maintenant détruire certain mur dans le monde \n Appuyer sur Z pour fermer le tutoriel.";
            //tuto après boss 1, meme principe que les précédent il s'affiche et lorsque la touche précisé dans le text est activé alors il s'enleve
            imageWall.GetComponent<Image>().DOFade(1, 0);
            imageWall2.GetComponent<Image>().DOFade(1, 0);

            if(Input.GetKeyDown(KeyCode.Z))
            {
                imageWall.GetComponent<Image>().DOFade(0, 0);
                imageWall2.GetComponent<Image>().DOFade(0, 0);
                textTuto.text = "";
                stepAfterBoss = false;
            }
        }
        if (stepAfterBoss2 == true)
        {
            //tuto après boss 2, meme principe que les précédent il s'affiche et lorsque la touche précisé dans le text est activé alors il s'enleve
            Debug.Log("activation tuto");
            textTuto.text = "vous pouvez maintenant vous transformez, appuyer sur S pour passer d'une forme à l'autre mais attention vos mouvements seront limités.";
            if (Input.GetKey(KeyCode.S))
            {
                textTuto.text = "";
                stepAfterBoss2 = false;
            }
        }
    }
    public void TutoAfterBoss()
    {
        //mort premier boss active tuto 1
        stepAfterBoss = true;
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        //collision box trigger après boss 2 qui spawn a sa mort
        if (other.gameObject.tag == "BoxTuto")
        {
            //active bool tuto 2
            Debug.Log("le tag !");
            stepAfterBoss2 = true;
            boxtuto.SetActive(false);
        }
    }
}
