﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public bool jumpBoost;
    private float moveSpeedPlayer = 7f;
    public float translationPlayer;
    private Rigidbody2D rbPlayer;
    private SpriteRenderer mySpriteRenderer;
    public float jumpPowerPlayer = 11f;
    public LayerMask layers;
    private Collider2D colliderPlayer;
    public Animator playerAnim;
    public BoxCollider2D boxPlayer;

    public bool smallSizeBuff;
    private Vector2 boxTaille, boxTaille2;
    public bool playerFormSphere;
    public Camera cameraJump;
    public GameObject player, camera;
    
    //ajouter 

    
    // Start is called before the first frame update
    void Start()
    {
        jumpBoost = false;
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        rbPlayer = transform.GetComponent<Rigidbody2D>();//recup component rigidbody2D
        colliderPlayer = transform.GetComponent<BoxCollider2D>();//recup component box collider
    }

    // Update is called once per frame
    void Update()
    {
        
        var transformPosition = camera.transform.position;
        transformPosition.x = player.transform.position.x;
        transformPosition.y = player.transform.position.y;
        playerAnim.SetFloat("Run", 0);
        
        transform.Translate(Input.GetAxisRaw("Horizontal") * moveSpeedPlayer * Time.deltaTime, 0f, 0f); //prend en compte dans l'input setting de Horizontal et avec l'input utilsé le deplacement va dans le negatif ou le positif
        translationPlayer = Input.GetAxisRaw("Horizontal") * moveSpeedPlayer;
        
        Vector3 characterScale = transform.localScale; //ligne pour changement de sens le sprite du joueur vers positif ou negatif scale
        
        if (Input.GetAxisRaw("Horizontal") < 0) //ligne pour changement de sens le sprite du joueur vers positif ou negatif scale
        {
            characterScale.x = -3.778f;
            playerAnim.SetFloat("Run",1);
        }
        if (Input.GetAxisRaw("Horizontal") > 0) //ligne pour changement de sens le sprite du joueur vers positif ou negatif scale
        {
            characterScale.x = 3.778f;
            playerAnim.SetFloat("Run",1);
        }
        transform.localScale = characterScale; //ligne pour changement de sens le sprite du joueur vers positif ou negatif scale
        if (playerFormSphere == false)
        {
            if (GroundCheck() && (Input.GetButtonDown("Jump")))//si pression sur button "Jump" et groundcheck ok alors
            {
                rbPlayer.velocity = Vector2.up * jumpPowerPlayer;//action jump player vers le haut
                cameraJump.DOShakePosition(0.1f, 0.02f, 100, 10, false);
                playerAnim.SetBool("Jump",true);
            }
        }
        
        if (Input.GetKeyDown(KeyCode.S) && smallSizeBuff == true)//si player appuie sur s après boss 2 alors
        {
            if (playerFormSphere == false)//si petite form pas activé alors joueur passe en petite form
            {
                Debug.Log("Ca fonctionne");
                boxPlayer.size = new Vector2(0.16f,0.16f);
                playerAnim.SetBool("Morph", true);
                playerFormSphere = true;
            }
            else if (playerFormSphere == true)//si petite form activé alors joueur passe en grande form
            {
                Debug.Log("Ca fonctionne + true");
                playerAnim.SetBool("Morph", false);
                boxPlayer.size = new Vector2(0.16f,0.36f);
                playerFormSphere = false;
            }
        }
        if (jumpBoost == true)//jump boost player
        {
            jumpPowerPlayer = 25;
            Debug.Log("+8");
        }
    }

    public void SmallSizeBuffPlayer()
    {
        smallSizeBuff = true;
    }

    bool GroundCheck()
    {
        float extendRaycast = 0.1f;// dépassement raycast sous les pied du player
        RaycastHit2D hit = Physics2D.Raycast(colliderPlayer.bounds.center, Vector2.down, colliderPlayer.bounds.extents.y + extendRaycast, layers);//Raycast du player pour détecter le ground qui porte le layers Ground
        Color colorRaycast;// creation var color pour le raycast
        if (hit.collider != null)// check collision du hit
        {
            playerAnim.SetBool("Jump",false);
            colorRaycast = Color.red;//plus utile mais change de couleur si active drawnray
        }
        else
        {
            colorRaycast = Color.green;//plus utile mais change de couleur si active drawnray
        }
        
        //Debug.DrawRay(colliderPlayer.bounds.center, Vector2.down * (colliderPlayer.bounds.extents.y + extendRaycast));  le debug drawn est foutu en invisible car le raycats fonctionne maintenant
        //Debug.Log("player "+ hit.collider);//debug log retour 
        return hit.collider != null;// return pour la void bool
        
    }

    public void JumpBoost()
    {
        jumpBoost = true;
        if (jumpBoost == true)
        {
            jumpPowerPlayer = 25;
            Debug.Log("+8");
        }
    }
}

