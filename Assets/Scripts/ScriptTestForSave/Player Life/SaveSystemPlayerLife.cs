﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveSystemPlayerLife
{
    public static void SavePlayerLife(PlayerLife playerLife)
    {
        BinaryFormatter formatters = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player.ok";
        FileStream stream = new FileStream(path, FileMode.Create);
        
        PlayerDataLife dataLife = new PlayerDataLife(playerLife);

        formatters.Serialize(stream, dataLife);
        stream.Close();
    }
    
    public static PlayerDataLife LoadPlayerLife()
    {
        string path = Application.persistentDataPath + "/player.ok";
        if (File.Exists(path))
        {
            BinaryFormatter formatters = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerDataLife dataLife = formatters.Deserialize(stream) as PlayerDataLife;

            stream.Close();
            
            return dataLife;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    } 
}
