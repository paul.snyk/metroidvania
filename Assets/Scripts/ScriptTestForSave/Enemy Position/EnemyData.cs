﻿[System.Serializable]
public class EnemyData
{
    public float[] positionEnemy;

    public EnemyData(EnemySave enemy) // Va dans le script enemy save afin d'y trouver les mêmes valeurs pour les enregistrer
    {
        positionEnemy = new float[2];
        positionEnemy[0] = enemy.transform.position.x;
        positionEnemy[1] = enemy.transform.position.y;
    }
}
