﻿using System.IO;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;

public static class TestSaveSystem
{
	public static void SaveEnemy(EnemySave enemy) // Créer un chemin d'accès afin de créer un fichier de sauvegarde
	{
		BinaryFormatter formatter = new BinaryFormatter();
		string path = Application.persistentDataPath + "/enemy.yes";
		FileStream stream = new FileStream(path, FileMode.Create);
        
		EnemyData datas = new EnemyData(enemy); // récupère les données à sauvegarder dans EnemyData
        
		formatter.Serialize(stream, datas);
		stream.Close(); //Ferme le FileStream
	}

	public static EnemyData LoadEnemy()
	{
		string path = Application.persistentDataPath + "/enemy.yes"; // Va dans le chemin que l'on vient de créer pour  récupérer les données
		if (File.Exists(path))
		{
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream stream = new FileStream(path, FileMode.Open); //Ouvre le FileStream

			EnemyData datas = formatter.Deserialize(stream) as EnemyData; // Cherche la dernière position de l'ennemi  
			stream.Close();
            
			return datas;
		}
		else
		{
			Debug.LogError("Save file not found in " + path); // Sinon il envoie un message d'erreur
			return null;
		}
	}
}