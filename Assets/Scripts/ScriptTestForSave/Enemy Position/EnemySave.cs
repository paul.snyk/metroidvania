﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySave : MonoBehaviour
{
	public EnemySave enemy;

	public void SaveEnemy() // Sauvegarde les coordonnées de l'ennemi
	{
		TestSaveSystem.SaveEnemy(this);
	}

	public void LoadEnemy() // récupère les datas du script Enemy data, qui enregistre la position de l'ennemi
	{
		EnemyData datas = TestSaveSystem.LoadEnemy();

		Vector2 positionEnemy;
		positionEnemy.x = datas.positionEnemy[0];
		positionEnemy.y = datas.positionEnemy[1];
		transform.position = positionEnemy;
	}

	public void Update() // Si on appuie sur A, sauvegarde les coordonnées de l'ennemi
	{
		if (Input.GetKeyDown(KeyCode.A))
		{
			TestSaveSystem.SaveEnemy(this);
		}

		if (Input.GetKeyDown(KeyCode.P)) //lance la coroutine
		{
			StartCoroutine(WaitForLoad());
		}
	}

	IEnumerator WaitForLoad() //Charge les derniers data de la position de l'ennemi
	{
		yield return new WaitForSeconds(2);
		EnemyData datas = TestSaveSystem.LoadEnemy();

		Vector2 positionEnemy;
		positionEnemy.x = datas.positionEnemy[0];
		positionEnemy.y = datas.positionEnemy[1];
		transform.position = positionEnemy;
	}
}