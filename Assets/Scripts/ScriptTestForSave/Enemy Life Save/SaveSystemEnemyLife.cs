﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveSystemEnemyLife
{
	public static void SaveLifeEnemy(EnemyLife enemyLifeSave)
	{
		BinaryFormatter formatterLife = new BinaryFormatter();
		string path = Application.persistentDataPath + "/enemy.yes";
		FileStream streamLife = new FileStream(path, FileMode.Create);
        
		EnemyLifeData dataLifeEnemy = new EnemyLifeData(enemyLifeSave);
        
		formatterLife.Serialize(streamLife, dataLifeEnemy);
		streamLife.Close();
	}

	public static EnemyLifeData LoadLifeEnemy()
	{
		string path = Application.persistentDataPath + "/enemy.yes";
		if (File.Exists(path))
		{
			BinaryFormatter formatterLife = new BinaryFormatter();
			FileStream stream = new FileStream(path, FileMode.Open);

			EnemyLifeData dataLifeEnemy = formatterLife.Deserialize(stream) as EnemyLifeData;
			stream.Close();
            
			return dataLifeEnemy;
		}
		else
		{
			Debug.LogError("Save file not found in " + path);
			return null;
		}
	}
}