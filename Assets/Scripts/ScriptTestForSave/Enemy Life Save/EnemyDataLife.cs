﻿[System.Serializable]
public class EnemyLifeData
{
	public int healthEnemy;
	public bool playerBuffForce;

	public EnemyLifeData(EnemyLife enemyLifeSave)
	{
		healthEnemy = enemyLifeSave.healthEnemy;
	}
}