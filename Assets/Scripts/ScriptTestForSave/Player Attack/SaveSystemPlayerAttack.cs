﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveSystemPlayerAttack
{
	public static void SavePlayerAttack(PlayerAttack playerAttack)
	{
		BinaryFormatter formatters = new BinaryFormatter();
		string path = Application.persistentDataPath + "/player.ok";
		FileStream stream = new FileStream(path, FileMode.Create);
        
		PlayerDataAttack dataAttack = new PlayerDataAttack(playerAttack);

		formatters.Serialize(stream, dataAttack);
		stream.Close();
	}
    
	public static PlayerDataAttack LoadPlayerAttack()
	{
		string path = Application.persistentDataPath + "/player.ok";
		if (File.Exists(path))
		{
			BinaryFormatter formatters = new BinaryFormatter();
			FileStream stream = new FileStream(path, FileMode.Open);

			PlayerDataAttack dataAttack = formatters.Deserialize(stream) as PlayerDataAttack;

			stream.Close();
            
			return dataAttack;
		}
		else
		{
			Debug.LogError("Save file not found in " + path);
			return null;
		}
	} 
}