﻿[System.Serializable]

public class PlayerDataAttack
{
	public bool playerBuffForce;
	public bool wallContact;
	public bool smallSizeBuff;
	
	public PlayerDataAttack(PlayerAttack playerAttack)
	{
		playerBuffForce = playerAttack.playerBuffForce;
		wallContact = playerAttack.wallContact;
		smallSizeBuff = playerAttack.smallSizeBuff;
	}
}
