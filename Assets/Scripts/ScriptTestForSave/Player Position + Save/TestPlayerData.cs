﻿[System.Serializable]
public class TestPlayerData
{
   public int save;
   public float[] position;
   public bool cantSave, jumpBoost, playerBuffForce;

   public TestPlayerData(TestPlayerSave player)
   {
      save = player.save;
      cantSave = player.cantSave;
      jumpBoost = player.jumpBoost;

      position = new float[2];
      position[0] = player.transform.position.x;
      position[1] = player.transform.position.y;
   }
}
