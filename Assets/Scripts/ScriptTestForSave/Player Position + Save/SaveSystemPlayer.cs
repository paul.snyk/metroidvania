﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveSystemPlayer
{
    
    public static void SavePlayer(TestPlayerSave player)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player.yes";
        FileStream stream = new FileStream(path, FileMode.Create);
        
        TestPlayerData data = new TestPlayerData(player);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static TestPlayerData LoadPlayer()
    {
        string path = Application.persistentDataPath + "/player.yes";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            TestPlayerData data = formatter.Deserialize(stream) as TestPlayerData;

            stream.Close();
            
            return data;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
}
