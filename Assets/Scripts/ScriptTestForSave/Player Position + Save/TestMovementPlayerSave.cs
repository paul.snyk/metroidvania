﻿using UnityEngine;

public class TestMovementPlayerSave : MonoBehaviour
{
    public Vector2 position;

    public float speed = 5;
    public GameObject player;
    // Update is called once per frame
   void Update()
    {
        position = player.transform.position;
        Debug.Log(position);
       
	    if (Input.GetKey(KeyCode.RightArrow))
	    {
                transform.Translate(Vector2.right * (speed * Time.deltaTime));
	    }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
                transform.Translate(Vector2.left * (speed * Time.deltaTime));
        }

        if (Input.GetKey(KeyCode.UpArrow)) 
        {
                transform.Translate(Vector2.up * (speed * Time.deltaTime));
        }

        if (Input.GetKey(KeyCode.DownArrow)) 
        {
                transform.Translate(Vector2.down * (speed * Time.deltaTime));
        }
    }
}
