﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TestPlayerSave : MonoBehaviour
{
	public int save = 5;
	public bool cantSave, jumpBoost, boolFlags;
	public TestPlayerSave player;
	public GameObject loadExplosion, flag;
	public Transform loadTransform;
	public TextMeshProUGUI saveText;
	public int numberFlag;
	
	public void Start()
	{
		cantSave = false;
		jumpBoost = false;
		boolFlags = true;
		saveText.text = save.ToString();
	}

	public void SavePlayer()
	{
		SaveSystemPlayer.SavePlayer(player);
		save--;
	}

	public void LoadPlayer()
	{
		TestPlayerData data = SaveSystemPlayer.LoadPlayer();
		save = data.save;
		cantSave = data.cantSave;
		jumpBoost = data.jumpBoost;
		save--;

		Vector2 position;
		position.x = data.position[0];
		position.y = data.position[1];
		transform.position = position;
	}

	public void Update()
	{
		//saveText.text = save.ToString();
		if (Input.GetKeyDown(KeyCode.A) && cantSave == false)
		{
			if (boolFlags == true)
			{
				SaveSystemPlayer.SavePlayer(player);
				save--;
				saveText.text = save.ToString();
				GameObject flags = Instantiate(flag, transform.position, Quaternion.identity);
				numberFlag--;
				Debug.Log("SAVEEEEEEEEEEEEE");
			}
			if (boolFlags == false)
			{
				Destroy (GameObject.FindWithTag("Flag"));
				boolFlags = true;
				numberFlag++;
				Debug.Log("DRAPEAUUUUUUUUU");
			}
		}

		if (numberFlag <= 0)
		{
			boolFlags = false;
		}
		
		if (save <= 0)
		{
			cantSave = true;
		}

		if (Input.GetKeyDown(KeyCode.P))
		{
			StartCoroutine(WaitForLoad());
			GameObject ps = Instantiate(loadExplosion, loadTransform.position, Quaternion.identity);
			Destroy(ps, 2);
		}
	}

	IEnumerator WaitForLoad()
	{
		yield return new WaitForSeconds(2);
		TestPlayerData data = SaveSystemPlayer.LoadPlayer();
		save = data.save;
		cantSave = data.cantSave;
		save--;
		saveText.text = save.ToString();

		Vector2 position;
		position.x = data.position[0];
		position.y = data.position[1];
		transform.position = position;
	}
}