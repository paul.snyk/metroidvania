﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class SkeletonEnemy : MonoBehaviour
{   
    // float
    public float speed, stoppingDistance, nearDistance, startTimeBtwShots;
    private float _timeBtwShots;
    // Transform
     public Transform player, enemy;

     // Start is called before the first frame update
    void Start() //Permet de savoir ou est la position du joueur
    {
	    player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update() // L'ennemi va suivre le player, il traversa même les murs, cependant il a une distance maximale à respecter
    {
         if (Vector2.Distance(transform.position, player.position) < nearDistance)
         {
                    transform.position = Vector2.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);
         } 
         else if (Vector2.Distance(transform.position, player.position) > stoppingDistance)
         {
                    transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
         }
         else if (Vector2.Distance(transform.position, player.position) < stoppingDistance && Vector2.Distance(transform.position, player.position) > nearDistance)
         {
                    transform.position = this.transform.position;
         }
         if (enemy.transform.position.x > player.transform.position.x) // Compare la position en x du player et de l'ennemi, il se tournera en fonction de la position du joueur
         {
	         transform.localScale  = new Vector3(-0.5f,0.5f,0.5f);
         } 
         else 
         {
	         transform.localScale  = new Vector3(0.5f,0.5f,0.5f);
         }
         /*if(_timeBtwShots <= 0) {
             //Instantiate(enemyaibullet, transform.position, Quaternion.identity);
             _timeBtwShots = startTimeBtwShots;
         } else 
         {
             _timeBtwShots -= Time.deltaTime;
         }*/
    }
}

