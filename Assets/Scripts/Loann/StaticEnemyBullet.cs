﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class StaticEnemyBullet : MonoBehaviour
{

    public float rotationSpeed, distance, fireRate;
    public Gradient redColor, greenColor;
    public GameObject bulletEnemy;
    public LineRenderer laser;

    private Camera _camera;
    private Rigidbody2D bulletEnemyrb, rb;
    private double timeStamp;
    
    // Start is called before the first frame update
    void Start()  // Permet au raycast de ne pas détecter les colliders dans lequel il est au start
    {
        Physics2D.queriesStartInColliders = false;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() // Permet une rotation sur lui même
    {
        transform.Rotate(Vector3.forward * (rotationSpeed * Time.deltaTime));
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position,transform.up, distance); // Envoie un Raycast qui commence en haut
        if (hitInfo.collider != null)
        {
            Debug.DrawLine(transform.position, hitInfo.point, Color.red); //Debug une DrawLine pour la voir dans la scène
            laser.SetPosition(1, hitInfo.point); // Change la distance du laser lorsqu'il touche un obstacle
            laser.colorGradient = redColor; // Change la couleur du laser quand il touche un obstacle
            if(hitInfo.collider.CompareTag("Player"))
            {
                if (timeStamp <= Time.time) // Instantie une bullet lorsque le cooldown est terminé
                {
                    Instantiate(bulletEnemy, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y), gameObject.transform.rotation); // Envoie la bullet en fonction de la position du laser
                    timeStamp = Time.time + fireRate;
                }
            }
        }
        else
        {
            Debug.DrawLine(transform.position, transform.position + transform.up * distance, Color.green); // Debug une ligne verte quand il ne touche rien
            laser.SetPosition(1, transform.position + transform.up * distance); // garde sa distance normale quand il ne touche rien
            laser.colorGradient = greenColor; // Le laser est vert quand il ne touche rien
        }
        laser.SetPosition(0, transform.position);
    }
}
