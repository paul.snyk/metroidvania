﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Patroller : MonoBehaviour
{
    public float speed, distance;
    private bool movingRight = true;

    public Transform groundDetection;
    void Update() //un raycast regarde continuellement en bas pour voir s'il y a encore un sol, s'il n'y en a pas, il se retourne
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, distance);
        if (groundInfo.collider == false)
        {
            if (movingRight == true)
            {
                transform.eulerAngles = new Vector3(0, -180,0 );
                movingRight = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0,0 );
                movingRight = true;
            }
        }
       
    }
}
