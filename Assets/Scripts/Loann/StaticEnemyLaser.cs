﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class StaticEnemyLaser : MonoBehaviour
{

	public float rotationSpeed, distance, laserdurationkill;
	public Gradient redColor, greenColor;
	public GameObject player;
	private Camera _camera;

    
	public LineRenderer laser;
	// Start is called before the first frame update
	void Start()
	{
		Physics2D.queriesStartInColliders = false;
	}

	// Update is called once per frame
	void Update() //Tout comme l'ennemi static avec bullet, il va rester sur lui même et tourner 
	{
		transform.Rotate(Vector3.forward * (rotationSpeed * Time.deltaTime));
		RaycastHit2D hitInfo = Physics2D.Raycast(transform.position,transform.right, distance);
		if (hitInfo.collider != null)
		{
			Debug.DrawLine(transform.position, hitInfo.point, Color.red);
			laser.SetPosition(1, hitInfo.point);
			laser.colorGradient = redColor;
			Wait();
			if(hitInfo.collider.CompareTag("Player") && laserdurationkill < 0) // si le joueur reste plus de 1 seconde dans le laser, il meurt
			{
				StartCoroutine(Wait());
			}

			if (hitInfo.collider.CompareTag("Player"))
			{
				laserdurationkill -= Time.deltaTime;
			}
		}
		else
		{
			Debug.DrawLine(transform.position, transform.position + transform.right * distance, Color.green);
			laser.SetPosition(1, transform.position + transform.right * distance);
			laser.colorGradient = greenColor;
		}

		if (hitInfo.collider.CompareTag("Obstacle")) // s'il touche un obstacle alors la durée du laser retourne à 1
		{
			laserdurationkill = 1;
		}
		laser.SetPosition(0, transform.position);
	}

	IEnumerator Wait()
	{
		yield return new WaitForSeconds(laserdurationkill);
		KillPlayer();
	}

	public void KillPlayer()
	{
		player.GetComponent<PlayerLife>().Die();
	}
}