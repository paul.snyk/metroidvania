﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class EnemyPoints : MonoBehaviour
{
    public float speed, startWaitTime;
    private float _waitTime;

    public Transform[] moveSpots;
    private int _randomSpot;
    
    // Start is called before the first frame update
    void Start() // Prends la valeur de startWaitTime et randomSpots choisi une valeur entre 0 et la taille de movespots que l'on a défini
    {
        _waitTime = startWaitTime;
        _randomSpot = Random.Range(0, moveSpots.Length);
    }

    // Update is called once per frame
    void Update() // L'ennemi va choisir au hasard un spot défini par un gameObject et il va y aller
    {
        transform.position = Vector2.MoveTowards(transform.position, moveSpots[_randomSpot].position, speed * Time.deltaTime);
        if (Vector2.Distance(transform.position, moveSpots[_randomSpot].position) < 0.2f) // Envoi un cooldown pour que l'ennemi change de position, la aussi aléatoirement
        {
            if (_waitTime <= 0)
            {
                _randomSpot = Random.Range(0, moveSpots.Length);
                _waitTime = startWaitTime;
            }
            else
            {
                _waitTime -= Time.deltaTime;
            }
        }
       
    }
}
