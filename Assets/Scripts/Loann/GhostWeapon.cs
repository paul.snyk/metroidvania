﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostWeapon : MonoBehaviour
{
    public GameObject explosionGhostWeapon;
    public void OnTriggerEnter2D(Collider2D col)
    {
        col.gameObject.GetComponent<PlayerLife>().DamagePlayer(1);
        GameObject ps = Instantiate(explosionGhostWeapon, transform.position, Quaternion.identity);
        Destroy(ps, 0.4f);
    }
}
