﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class EnemyLife : MonoBehaviour
{
    public int healthEnemy, maxhealthEnemy, enemyKilleds;

    public GameObject healthBarUIEnemy, explosion;
    public Camera mainCamera;

    public Slider sliderEnemy;

    //public EnemyLife enemyLife;
    
    // Start is called before the first frame update
    void Start()
    {
        healthEnemy = maxhealthEnemy;
        healthBarUIEnemy.SetActive(false);
        sliderEnemy.value = CalculateValueEnemy();
    }

    // Update is called once per frame
    void Update() // Si on appuie sur P ca charge la dernière sauvegarde, sur A ca sauvegarde
    {
        sliderEnemy.value = CalculateValueEnemy();
        if (Input.GetKeyDown(KeyCode.P))
        {
            StartCoroutine(WaitForLoad());
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            SaveSystemEnemyLife.SaveLifeEnemy(this);
        }
        if (healthEnemy < maxhealthEnemy)
        {
            healthBarUIEnemy.SetActive(true);
        }
        
        if (healthEnemy <= 0)
        {
            Die();
        }
        
        if (healthEnemy > maxhealthEnemy)
        {
            healthEnemy = maxhealthEnemy;
        }
    }
    
    public void Damage(int enemyDamage)  // Tremblement de caméra + explosion + scale de l'ennemi lors de la mort
    {
        healthEnemy -= enemyDamage; 
        healthBarUIEnemy.SetActive(true);
        sliderEnemy.value = CalculateValueEnemy();
        Debug.Log("Aieuh");
        StartCoroutine(Blink());
    }

    public void Die() // Tremblement de caméra + explosion + scale de l'ennemi lors de la mort
    {
        mainCamera.DOShakePosition (0.5f, 0.05f, 100, 90, false);
        mainCamera.DOShakeRotation(0.5f, 0.05f, 100, 90, false);
        explosion.SetActive(true);
        transform.DOScale(0, 0.25f);
        StartCoroutine(WaitUntilDie());
        GameObject explode = Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(explode, 1f);
        EnemyKilleds();
        Destroy(gameObject);
    }
    IEnumerator Blink() // Change la couleur du sprite en rouge pendant 0.2 secondes quand l'ennemi se fait frapper
    {
        GetComponent<SpriteRenderer>().color = new Color(1, 0, 0);
        yield return new WaitForSeconds(0.2f);
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
    }

    IEnumerator WaitUntilDie()
    {
        yield return new WaitForSeconds(0.25f);
        Die();
    }

    float CalculateValueEnemy() // retourne la valeur de la vie de l'ennemi
    {
        return healthEnemy;
    }

    public void EnemyKilleds() //ajoute un à la variable enemyKilleds
    {
        enemyKilleds ++;
    }

    public void SaveLifeEnemy()
    {
        SaveSystemEnemyLife.SaveLifeEnemy(this);
        sliderEnemy.value = CalculateValueEnemy();
    }

    public void LoadLifeEnemy()
    {
        EnemyLifeData enemyLifeSave = SaveSystemEnemyLife.LoadLifeEnemy();
        
        healthEnemy = enemyLifeSave.healthEnemy;
        sliderEnemy.value = CalculateValueEnemy();
    }

    IEnumerator WaitForLoad() // Attends 2 secondes avant de charger la dernière sauvegarde
    {
        yield return new WaitForSeconds(2);
        EnemyLifeData enemyLifeSave = SaveSystemEnemyLife.LoadLifeEnemy();

        healthEnemy = enemyLifeSave.healthEnemy;
        sliderEnemy.value = CalculateValueEnemy();
    }
}
